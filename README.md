# Project NetWalker

Proof-of-concept web browser using [NW.js](https://nwjs.io) and Chromium

This is just a proof-of-concept to see how many features I can add ~~before I get a cease and desist letter from Google~~ to make it function like a regular Chromium-based browser

## Building

### Prerequisites

- NW.js
- Node.js

### Instructions

1. [Get NW.js](https://nwjs.readthedocs.io/en/latest/For%20Users/Getting%20Started/#get-nwjs)
2. `git clone https://gitlab.com/nikkehtine/netwalker`
3. Move to the cloned directory
   - Run `npm run dev` to debug the project
   - Run `npm run build` to build the release version
